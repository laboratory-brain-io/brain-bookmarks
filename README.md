# Brain-API v1 - Web api for Brain
Brainの社内用WebAPI。  
仕様策定中。

## DESCRIPTION
Brain社内で情報共有等を行う為のWebAPI。  
ユーザ登録・ブックマーク共有といった機能を実装予定。

## USAGE
### API Objects
- [**Users** \- ユーザリソース操作](https://bitbucket.org/laboratory-brain-io/brain-bookmarks/src/HEAD/document/api/users/README.md)
- [**Bookmarks** \- ブックマークリソース操作](https://bitbucket.org/laboratory-brain-io/brain-bookmarks/src/HEAD/document/api/bookmarks/README.md)

## LICENSE
MIT

## AUTHOR
- Shotaro Yamashita
- Masahiko Sato
- Minoru Yoshida
