# Brain-Bookmark-API v1 - Bookmark api for Brain
Webサイトをクリップし、ブックマークとして共有等を行う為のAPI。

## ENDPOINTS
### Get [POST]
ブックマークリソースの取得

    /api/bookmarks/get


#### Parameters
 name  | type    | required | default | comments
-------|---------|----------|---------|-----------------------------------------------------
 ids   | String  | no       |         | リソースIDを指定して戻り値のフィルタリングを行う。カンマ区切りで複数指定可。
 users | String  | no       |         | ユーザ名を指定して戻り値のフィルタリングを行う。カンマ区切りで複数指定可。**me**を指定で自身のリソースを取得。
 tags  | String  | no       |         | タグ名を指定して戻り値のフィルタリングを行う。カンマ区切りで複数指定可。
 items | Number  | no       | 50      | リソースの取得件数。最大値は**1000**。

#### Return

    {
      "status": "<Status code>",
      "total": <Total value of items>,
      "results": [
        {
          "id": "<ID of resource>",
          "user": "<ID of user had added bookmark resource>",
          "url": "<Url of resource>",
          "title": "<Title of resource>",
          "tags": [
            "<Tag name of resource>"
          ]
        }
      ]
    }


### Add [POST]
ブックマークリソースの追加

    /api/bookmarks/add


#### Parameters
 name  | type    | required | default | comments
-------|---------|----------|---------|-----------------------------------------------------
 url   | String  | yes      |         | リソースのURL。
 title | String  | yes      |         | リソースのタイトル。
 tags  | String  | no       |         | リソースに付与するタグ名。カンマ区切りで複数指定可。

#### Return

    {
      "status": "<Status code>",
      "result": {
        "url": "<Url of resource>",
        "title": "<Title of resource>",
        "tags": [
          "<Tag name of resource>"
        ]
      }
    }


### Edit [POST]
ブックマークリソースの修正・変更

    /api/bookmarks/edit


#### Parameters
 name  | type    | required | default | comments
-------|---------|----------|---------|-----------------------------------------------------
 id    | String  | yes      |         | リソースのID。
 url   | String  | yes      |         | リソースのURL。
 title | String  | yes      |         | リソースのタイトル。
 tags  | String  | no       |         | リソースに付与するタグ名。カンマ区切りで複数指定可。

#### Return

    {
      "status": "<Status code>",
      "result": {
        "id": "ID of resource",
        "url": "<Url of resource>",
        "title": "<Title of resource>",
        "tags": [
          "<Tag name of resource>"
        ]
      }
    }


### Del [POST]
ブックマークリソースの削除

    /api/bookmarks/del


#### Parameters
 name  | type    | required | default | comments
-------|---------|----------|---------|-----------------------------------------------------
 ids   | String  | yes      |         | リソースのID。カンマ区切りで複数指定可。

#### Return

    {
      "status": "<Status code>",
      "result": {
        "ids": [
          "ID of resource"
        ]
      }
    }


## STATUS
 code | detail
------|-----------------------------------------------------
 200  | 成功
 400  | 失敗
 500  | アプリケーションエラー
