# Brain-Users-API v1 - Users api for Brain
社内APIのユーザ関連リソースの操作を行うAPI。  
ユーザ登録・削除等の機能の他、ログイン・ログアウト機能を提供する。

## ENDPOINTS
### Login [POST]
ログインを行う

    /api/users/login


#### Parameters
 name     | type    | required | default | comments
----------|---------|----------|---------|-----------------------------------------------------
 name     | String  | yes      |         | ユーザ名。半角英数字のみ有効。
 password | String  | yes      |         | ユーザパスワード。半角英数字、半角記号のみ有効。

#### Return

    {
      "status": "<Status code>"
    }


### Logout [POST]
ログアウトを行う

    /api/users/logout


#### Parameters
パラメータ不要。

#### Return

    {
      "status": "<Status code>"
    }


### Register [POST]
ユーザリソースの登録

    /api/users/register


#### Parameters
 name     | type    | required | default | comments
----------|---------|----------|---------|-----------------------------------------------------
 name     | String  | yes      |         | ユーザ名。半角英数字のみ有効。
 password | String  | yes      |         | ユーザパスワード。半角英数字、半角記号のみ有効。
 email    | String  | yes      |         | メールアドレス。半角英数字、半角記号のみ有効。

#### Return

    {
      "status": "<Status code>",
      "result": {
        "name": "<Name of user>",
        "email": "<Email of user>"
      }
    }


### Update [POST]
ユーザリソースの情報を更新

    /api/users/update


#### Parameters
 name     | type    | required | default | comments
----------|---------|----------|---------|-----------------------------------------------------
 name     | String  | no       |         | ユーザ名。半角英数字のみ有効。
 password | String  | no       |         | ユーザパスワード。半角英数字、半角記号のみ有効。
 email    | String  | no       |         | メールアドレス。半角英数字、半角記号のみ有効。

#### Return

    {
      "status": "<Status code>",
      "result": {
        "name": "<Name of user>",
        "email": "<Email of user>"
      }
    }


### Account [POST]
ユーザリソースの情報取得

    /api/users/account


#### Parameters
パラメータ不要。

#### Return

    {
      "status": "<Status code>",
      "result": {
        "name": "<Name of user>",
        "email": "<Email of user>"
      }
    }


### Unregister [POST]
ユーザリソースの登録解除

    /api/users/unregister


#### Parameters
パラメータ不要。

#### Return

    {
      "status": "<Status code>"
    }


## STATUS
 code | detail
------|-----------------------------------------------------
 200  | 成功
 400  | 失敗
 500  | アプリケーションエラー
