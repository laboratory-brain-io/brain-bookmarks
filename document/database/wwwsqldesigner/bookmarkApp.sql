



-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'users'
-- 
-- ---

DROP TABLE IF EXISTS `users`;
    
CREATE TABLE `users` (
  `id` INTEGER NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `mail` VARCHAR(255) NOT NULL,
  `delete_flag` TINYINT NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'bookmarks'
-- 
-- ---

DROP TABLE IF EXISTS `bookmarks`;
    
CREATE TABLE `bookmarks` (
  `id` INTEGER NULL AUTO_INCREMENT,
  `user_id` INTEGER NOT NULL,
  `url` MEDIUMTEXT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `tags` MEDIUMTEXT NULL,
  `delete_flag` TINYINT NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'comments'
-- 
-- ---

DROP TABLE IF EXISTS `comments`;
    
CREATE TABLE `comments` (
  `id` INTEGER NULL AUTO_INCREMENT,
  `book_id` INTEGER NULL,
  `body` MEDIUMTEXT NULL,
  `delete_flag` TINYINT NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'stars'
-- 
-- ---

DROP TABLE IF EXISTS `stars`;
    
CREATE TABLE `stars` (
  `id` INTEGER NULL AUTO_INCREMENT,
  `book_id` INTEGER NULL,
  `user_id` INTEGER NULL,
  `delete_flag` TINYINT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `bookmarks` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
ALTER TABLE `comments` ADD FOREIGN KEY (id) REFERENCES `bookmarks` (`id`);
ALTER TABLE `stars` ADD FOREIGN KEY (book_id) REFERENCES `bookmarks` (`id`);
ALTER TABLE `stars` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `users` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `bookmarks` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `comments` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `stars` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `users` (`id`,`name`,`password`,`mail`,`delete_flag`,`created`,`modified`) VALUES
-- ('','','','','','','');
-- INSERT INTO `bookmarks` (`id`,`user_id`,`url`,`title`,`tag`,`delete_flag`,`created`,`modified`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `comments` (`id`,`book_id`,`body`,`delete_flag`,`created`,`modified`) VALUES
-- ('','','','','','');
-- INSERT INTO `stars` (`id`,`book_id`,`user_id`,`delete_flag`,`created`,`modified`) VALUES
-- ('','','','','','');


